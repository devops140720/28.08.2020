import random

def print_run_random(from_number, to_number, skip):
    for digit in range(from_number, to_number, skip):
        random_num_app = random.randint(1, 20)
        print(f'{digit}'*random_num_app, end="")

def write_IO_run_random(fhw, from_number, to_number, skip, max_rep):
    '''
     write digits to file handler in range loop from [from_number] to [to_number]
    :param fhw: file handler with writing abilities
    :param from_number: start range
    :param to_number: end range
    :param skip: skip in range
    :return: None
    '''
    for digit in range(from_number, to_number, skip):
        random_num_app = random.randint(1, max_rep)
        fhw.write(f'{digit}'*random_num_app)

#run_random(0, 10, 1)
#print('TREASURE', end="")
#run_random(9, -1, -1)

def get_int_number(msg):
    while True:
        a_number = input(msg)
        if a_number.isdigit():
            break
    number_int = int(a_number)
    return number_int

def print_menu():
    print('Please choose:')
    print('1- go forward')
    print('2- go backwards')
    print('3- leave in shame')

def max_file_position(fh):
    fh.seek(0,2)
    max = fh.tell()
    fh.seek(0) # bring cursor to start
    return max

# ==============================================================================================================
# ==============================================================================================================
# ==============================================================================================================

map_file_locaion = 'c:/temp/map.txt';
highscore_file_locaion = 'c:/temp/highscore.txt';
maximum_number_of_digit_repitition = 10

print('creating random map ...')
with open(map_file_locaion, 'w') as map_file_handler_w:
    write_IO_run_random(map_file_handler_w, 0, 10, 1, maximum_number_of_digit_repitition)
    map_file_handler_w.write('TREASURE')
    write_IO_run_random(map_file_handler_w, 9, -1, -1, maximum_number_of_digit_repitition)

print(f'random map created at {map_file_locaion}...')

print('Starting game:')

win = False
steps = 0

with open(map_file_locaion, 'r') as file_handler_map_r:
    my_position = 0 # initial position
    max = max_file_position(file_handler_map_r) # max position of file

    # main gameplay loop
    while True:
        print_menu()

        # input numbers until user writes 1,2 or 3
        while True:
            user_option = input("choose: ")
            if user_option in ["1","2","3"]:
                break

        # user exit option
        if user_option == "3":
            break

        steps = steps + 1

        # user forward option
        if user_option == "1":
            jump = get_int_number("how much to jump forward? ")
            my_position = my_position + jump
            if my_position > max:
                my_position = max

        # user backwards option
        if user_option == "2":
            jump = get_int_number("how much to jump backwards? ")
            my_position = my_position - jump
            if my_position < 0:
                my_position = 0

        # move position to user choice
        file_handler_map_r.seek(my_position)

        # print the character of the current location
        one_char_from_file = file_handler_map_r.read(1)
        print(f'you are here : ---- {one_char_from_file} ----')

        # check if user step on treasure
        if one_char_from_file in "TREASURE":
            win = True
            break

# was the game over becuase user exit? or because user won?
if win == True:
    # user won
    print(f'You won!!! it took you {steps} steps')
    # write to high score
    with open(highscore_file_locaion, 'a') as high_score_fh_a:
        user_name = input("player, what's your name? ")
        high_score_fh_a.write(f'=== {user_name} : {steps} steps ===\n')
else:
    # user quit
    print('Game over quitter !')

