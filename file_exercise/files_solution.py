def GetFileSize(file_location):
    with open(file_location, 'r') as fh:
        fh.seek(0, 2) # move to end of file
        size = fh.tell()
    return size

def GetAllSizes(list_of_locations):
    d = dict()
    # ['c:/temp/hello.txt','c:/temp/goodbye.txt']
    for one_location in list_of_locations:
        size = GetFileSize(one_location)
        d[one_location] = size
    return d

def GetSumSize(list_of_locations):
    d = GetAllSizes(list_of_locations)
    sum_of_file_sizes = sum(d.values())
    return sum_of_file_sizes

def GetWordsFromFile(file_location):
    words_no_dup = set()
    with open(file_location, 'r') as fh:
        for line in fh.readlines():
            # as we continued on gobbling
            # ['as','we','continued','on','gobbling']
            for word in line.split():
                words_no_dup.add(word)
    return words_no_dup

def FindWordInFile(file_location, word_find):
    with open(file_location, 'r') as fh:
        for line in fh.readlines():
            if word_find in line.split():
                return True
    return False

d = GetAllSizes(['c:/temp/hello.txt','c:/temp/goodbye.txt'])
print(d)
s1 = GetSumSize(['c:/temp/hello.txt','c:/temp/goodbye.txt'])
print(s1)
res = GetWordsFromFile('c:/temp/recip.txt')
print(sorted(res))